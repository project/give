<?php

namespace Drupal\give\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Format a donation amount with the right currency.
 *
 * @todo Replace this with the currency module.
 */
#[FieldFormatter(
  id: 'give_cents_to_dollars',
  label: new TranslatableMarkup('Donation amount'),
  description: new TranslatableMarkup('Convert stored cents into dollars or whatever'),
  field_types: ['integer']
)]
class CentsToDollarsFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => $items->getEntity()->getFormattedAmount()
      ];
    }
    return $elements;
  }

}
